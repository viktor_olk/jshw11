let removeErVal = (item) => {
  item.classList.remove("valid");
  item.classList.remove("error");
};

let resElemVal = (elem) => {
  elem.value = "";
};

let classAdd = (item, className) => {
  item.classList.add(className);
};

let classRemove = (item, className) => {
  item.classList.remove(className);
};

export { removeErVal, resElemVal, classAdd };
