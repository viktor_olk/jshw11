let exOneBlur = (e) => {
  if (
    e.target.dataset.length < e.target.value.length ||
    e.target.value.length === 0
  ) {
    e.target.classList.remove("valid");
    e.target.classList.add("error");
  } else if (e.target.value.length <= e.target.dataset.length) {
    e.target.classList.remove("error");
    e.target.classList.add("valid");
  }
};

export { exOneBlur };
