import {
  removeErVal,
  resElemVal,
  classAdd,
} from "./modules/common_functions.js";
import { exOneBlur } from "./modules/ex1_functions.js";

window.addEventListener("DOMContentLoaded", () => {
  // Variables of exercise 1 ----------------------------
  const exercise1 = document.getElementById("exercise_1"),
    ex1Title = document.querySelector("#exercise_1 h2"),
    ex1Content = document.querySelector("#exercise_1 .input-box-1"),
    [...inputs] = document.querySelectorAll(".input-box-1>input");

  // Varables of exercise 2 -----------------------------
  const exercise2 = document.getElementById("exercise_2"),
    ex2Title = document.querySelector("#exercise_2 h2"),
    ex2Content = document.querySelector(".ex2-content"),
    ex2Input = document.getElementById("price"),
    priceTxt = document.getElementById("price-txt"),
    priceError = document.getElementById("price-error"),
    reset = document.getElementById("reset"),
    [...spans] = document.querySelectorAll(".ex2-content span");
  let val = "";

  // Ч1 :
  // Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

  inputs.forEach((input) => {
    input.addEventListener("blur", exOneBlur);
  });

  exercise1.addEventListener("click", () => {
    ex1Title.classList.add("display-none");
    ex1Content.classList.remove("display-none");
    ex2Title.classList.remove("display-none");
    ex2Content.classList.add("display-none");
    ex2Input.classList.remove("green-font");
    // ----------------------------
    spans.forEach((span) => classAdd(span, "display-none"));
    resElemVal(ex2Input);
    removeErVal(ex2Input);
  });

  // Ч2:
  // - При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
  // - Поведінка поля має бути такою:
  // - При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
  // - Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст:
  // .
  // Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
  // - При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
  // - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою,
  // під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
  //   const spanUp = document.createElement("span");
  //   spanUp.classList.add("position");

  ex2Input.addEventListener("focus", () => {
    ex2Input.classList.add("valid");
    ex2Input.classList.remove("error");
    priceError.classList.add("display-none");
    ex2Input.classList.remove("green-font");
    ex2Input.addEventListener("change", () => {
      val = Math.round(ex2Input.value * 100) / 100;
    });
  });

  ex2Input.addEventListener("blur", () => {
    ex2Input.classList.remove("valid");
    if (val > 0) {
      ex2Input.value = val;
      ex2Input.classList.add("green-font");
      ex2Input.classList.remove("error");
      priceTxt.innerText = `Price: ${val} USD.`;
      priceTxt.classList.remove("display-none");
      reset.classList.remove("display-none");
    } else {
      ex2Input.classList.add("error");
      priceError.classList.remove("display-none");
      priceTxt.classList.add("display-none");
      reset.classList.remove("display-none");
      ex2Input.classList.remove("green-font");
    }

    reset.addEventListener("click", () => {
      val = "";
      resElemVal(ex2Input);
      ex2Input.classList.remove("valid");
      ex2Input.classList.remove("error");
      priceTxt.classList.add("display-none");
      reset.classList.add("display-none");
      priceError.classList.add("display-none");
      ex2Input.classList.remove("green-font");
    });
  });

  exercise2.addEventListener("click", () => {
    ex1Title.classList.remove("display-none");
    ex1Content.classList.add("display-none");
    ex2Title.classList.add("display-none");
    ex2Content.classList.remove("display-none");
    // -----------------------------------------------
    inputs.forEach((input) => {
      removeErVal(input);
      resElemVal(input);
    });
  });
});
